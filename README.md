# Flux Engine News

Free and balanced news for you!

Page: https://news.fluxengine.ltd

## Python

```sh
poetry config virtualenvs.in-project true
poetry install
poetry run python main.py
curl -L https://github.com/getzola/zola/releases/download/$ZOLA_VERSION/zola-$ZOLA_VERSION-x86_64-unknown-linux-gnu.tar.gz | tar xz
chmod +x zola && ./zola build
```
