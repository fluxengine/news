#!/usr/bin/python

import asyncio
import re
import xml.etree.ElementTree as ET
from datetime import datetime
from hashlib import md5

from dateutil.parser import parse
from dateutil.parser._parser import ParserError
import feedparser
from requests import get


RSS_FEED = "https://git.sr.ht/~dark0dave/rss_feed/blob/main/backup.opml"
DEFAULT_DATE = datetime.utcnow()


async def get_content(url: str) -> list[str]:
    feeds_xml = get(url).text.strip()
    return [feed for feed in feedparser.parse(feeds_xml).entries]


def parse_date(page) -> str:
    try:
        date = parse(page.get("published", page.get("updated", str(DEFAULT_DATE))))
    except ParserError:
        date = DEFAULT_DATE
    return date.strftime("%Y-%m-%d %H:%m:%S")


async def generate_page(source: str, page) -> None:
    title = re.sub("[^A-Za-z0-9- ]", "", page["title"].replace("\n", ""))
    date = parse_date(page)
    content = f"""+++
title = "{title}"
date = "{date}"
[extra]
url = "{page.get("link", "")}"
source = "{source}"
+++
"""
    page_name = title.strip().replace(" ", "-").lower()
    hash_page_name = md5(page_name.encode("utf-8")).hexdigest()
    filename = f"content/news_{hash_page_name}.md"
    with open(filename, "a+") as file:
        file.write(content)


async def generate_link(links) -> tuple[str, list[str]]:
    return (links["title"], await get_content(links["xmlUrl"]))


async def main() -> None:
    feeds_xml = get(RSS_FEED).text.strip()
    urls = [feed.attrib for feed in ET.fromstring(feeds_xml)[1]]
    news_links = await asyncio.gather(*[generate_link(link) for link in urls])
    await asyncio.gather(
        *[
            generate_page(source, article)
            for (source, articles) in news_links
            for article in articles
        ]
    )


if __name__ == "__main__":
    asyncio.run(main())
